<!DOCTYPE html>
<html>
    <head>
        <title>Register SanberBook</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
    <body>
        <div>
            <h1>Buat Account Baru!</h1>
        </div>
        <div>
            <h3>Sign Up Form</h3>
            <form action="/welcome" method="post">
                @csrf
                <label for="fname">First Name :</label>
                <p><input type="text" name="fname" id="fname"></p>

                <label for="lname">Last Name :</label>
                <p><input type="text" name="lname" id="lname"></p>

                <label>Gender :</label>
                <p>
                    <input type="radio" name="gender" value="Male"> Male <br/>
                    <input type="radio" name="gender" value="Female"> Female <br/>
                    <input type="radio" name="gender" value="Other"> Other
                </p>

                <label>Nationality :</label>
                <p>
                <select name="Nationality">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Singaporean">Singaporean</option>
                    <option value="Malaysian">Malaysian</option>
                    <option value="Austalian">Austalian</option>
                </select>
                </p>

                <label >Language Spoken :</label>
                <p>
                    <input type="checkbox" name="lang" value="Bahasa">Bahasa Indonesia<br/>
                    <input type="checkbox" name="lang" value="English">English<br/>
                    <input type="checkbox" name="lang" value="Others">Other
                </p>

                <label for="bio">Bio :</label><br/>
                <br><textarea cols="30" rows="10" id="bio"></textarea><br/>

                <button type="submit" value="Sign up" formaction="welcome">Sign Up</button>
            </form>
        </div>
    </body>
</html>