<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function handleform(Request $request){
        //dd($request->all());
        $fname = $request["fname"];
        $lname = $request["lname"];
        return view('welcome2', compact(['fname', 'lname']));
    }
}
